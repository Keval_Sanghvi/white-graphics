/**
 * scripts.js
 * Contains Script for basic static website named "White Graphics"
 */

$(window).on('load', function () {
    new WOW().init();
    $("#preloader").delay(1000).fadeOut('slow');

    /*Team Section*/
    $("#team-members").owlCarousel({
        items: 2,
        autoplay: true,
        loop: true,
        nav: true,
        navText: ['<i class="lni lni-chevron-left-circle"></i>', '<i class="lni lni-chevron-right-circle"></i>'],
        dots: false,
        smartSpeed: 700,
        margin: 20
    });

    /*Testimonials Section*/
    $("#testimonial-slider").owlCarousel({
        items: 1,
        loop: true,
        nav: true,
        smartSpeed: 700,
        dots: false,
        navText: ['<i class= "lni lni-chevron-left-circle"></i>', '<i class= "lni lni-chevron-right-circle"></i>']
    });

    /*Progress Bar*/
    $("#progress-elements").waypoint(function () {
        $(".progress-bar").each(function () {
            $(this).animate({
                width: $(this).attr("aria-valuenow") + "%"
            }, 800);
        });

        this.destroy(); //deletes the waypoint object binded with progress-elem,ents
        // so that it only animates once.
    }, {
        offset: 'bottom-in-view'
    });

    /*Clients Section*/
    $("#clients-elements").owlCarousel({
        items: 6,
        autoplay: true,
        loop: true,
        margin: 50,
        nav: true,
        navText: ['<i class="lni lni-chevron-left-circle"></i>', '<i class="lni lni-chevron-right-circle"></i>'],
        smartSpeed: 700,
        dots: false,
        responsive: {
            0: {
                items: 2
            },
            480: {
                items: 3
            },
            768: {
                items: 6
            }
        }
    });

    /*Service Tabs*/
    $("#services-tabs").responsiveTabs({
        animation: 'slide'
    });

    /*Portfolio Section*/
    $("#isotope-container").isotope({
        filter: '*'
    });

    $("#isotope-filters").on("click", "button", function () {
        let filterValue = $(this).attr('data-filter');

        $("#isotope-container").isotope({
            filter: filterValue
        });

        $("#isotope-filters").find(".active").removeClass("active");
        $(this).addClass("active");
    });

    $("#portfolio-wrapper").magnificPopup({
        delegate: "a",
        type: "image",
        mainClass: "mfp-fade",
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true,
            duration: 300,
            easing: 'ease-in-out',
            opener: function (openerElement) {
                return openerElement.is('img') ? openerElement :
                    openerElement.find('img');
            }
        }
    });

    // Stats Section
    $('.counter').counterUp({
        delay: 10,
        time: 2000
    });

    (function () {
        var addressString = "103, Evergreen CHS., <br>Airoli, Maharashtra, <br>India";
        var myLatLng = {
            lat: 19.173829,
            lng: 72.953716
        };

        var myMap = new google.maps.Map(document.getElementById('map'), {
            zoom: 17,
            center: myLatLng
        });

        var marker = new google.maps.Marker({
            position: myLatLng,
            map: myMap,
            title: "Click to See Address!"
        });

        var infoWindow = new google.maps.InfoWindow({
            content: addressString
        });

        marker.addListener('click', function () {
            infoWindow.open(myMap, marker);
        });
    })();
});

$(function () {
    showHideNav();
    $("#back-to-top").fadeOut();

    $(window).scroll(function () {
        showHideNav();
    });

    function showHideNav() {
        if ($(window).scrollTop() > 50) {
            $("nav").addClass("scrolled-navbar green-nav-top");
            $(".navbar-brand img").attr('src', "img/logo/logo-dark.png");
            $(".site-nav-wrapper").css("padding", "0px");
            $("#back-to-top").fadeIn();
        } else {
            $("nav").removeClass("scrolled-navbar green-nav-top");
            $(".navbar-brand img").attr('src', "img/logo/logo.png");
            $(".site-nav-wrapper").css("padding", "20px 0");
            $("#back-to-top").fadeOut();
        }
    }

    $("#mobile-nav-open-btn").click(function () {
        $("#mobile-nav").css("height", "100%");
    });

    $("#mobile-nav-close-btn, #mobile-nav a").click(function () {
        $("#mobile-nav").css("height", "0%");
    });

    $("a.smooth-scroll").click(function (event) {
        event.preventDefault();
        var section_id = $(this).attr("href");
        $("html, body").animate({
            scrollTop: $(section_id).offset().top + 30
        }, 1250, "easeInOutExpo")
    });
});
